
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from app import db
from sqlalchemy import inspect

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True)
    password = db.Column(db.String(80))
    name = db.Column(db.String(50))
    city = db.Column(db.String(50))
    is_admin = db.Column(db.Boolean,default=False)

    def set_password(self, password):
        """Set password"""
        self.password = generate_password_hash(password, method='sha256')

    def check_password(self, value):
        """Check password."""
        return check_password_hash(self.password, value)


    def get_id(self):
        return self.id

    def __repr__(self):
        return self.name

class Signal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    str_signal = db.Column(db.String(150), unique=True)
    hospital_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User')                                  #it'sno important now 12.03.18 15:59
    address = db.Column(db.String(150), unique=True)

    def get_id(self):
        return self.id

    def __repr__(self):
        return "Signal "+str(self.id)

class Income_signals(db.Model):

    id = db.Column(db.Integer, primary_key=True,unique=True)
    signal_id = db.Column(db.Integer, db.ForeignKey('signal.id'),unique=False)    
    hospital_id = db.Column(db.Integer, db.ForeignKey('user.id'),unique=False)
    date = db.Column(db.Date, nullable=False,unique=False)
    time = db.Column(db.String(50), nullable=False,unique=False)
    address = db.Column(db.String(150), unique=False)
    accepted = db.Column(db.Boolean, default=False)
    user = db.relationship('User',foreign_keys=[hospital_id])                                  #it'sno important now 12.03.18 15:59    
    signal = db.relationship('Signal',foreign_keys=[signal_id,address])
    def get_id(self):
        return self.id

    def toDict(self):
        return { c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs }
    
