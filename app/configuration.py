
class Config(object):
	"""
	Configuration base, for all environments.
	"""
	DEBUG = False
	TESTING = False
	SQLALCHEMY_DATABASE_URI = 'sqlite:///../database.db'
	BOOTSTRAP_FONTAWESOME = True
	SECRET_KEY = "Thisissupposedtobesecret!"
	CSRF_ENABLED = True

class ProductionConfig(Config):
	SQLALCHEMY_DATABASE_URI = 'sqlite:///../database.db'

class DevelopmentConfig(Config):
	DEBUG = False

class TestingConfig(Config):
	TESTING = True
