function myMap() {        
    var mapCanvas = document.getElementById("map");
    var mapOptions = {
    center: new google.maps.LatLng(43.2438949, 76.919709), 
    zoom: 13.1
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);  

    var myLatlng1 = new google.maps.LatLng(43.2358081, 76.9103184);

    var myLatlng2 = new google.maps.LatLng(43.251415,76.94530166);
    
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    var marker = new google.maps.Marker({
    position: myLatlng1,
    title: "{{ name }}"
    });
    var marker2 = new google.maps.Marker({
    position: myLatlng2,
    title: "5 Hospital"
    });
    // To add the marker to the map, call setMap();
    marker.setMap(map);   
    marker2.setMap(map);               
}
