var monthly = document.getElementById("monthly_chart").getContext('2d');
var myChart = new Chart(monthly, {
    type: 'bar',
    data: {
        labels: ["Device 1", "Device 2", "Device 3"],
        datasets: [{
            label: '',
            data: [4, 3, 1],
            backgroundColor: [
                'rgba(175, 238, 38, 0.8)',
                'rgba(64, 224, 208, 0.8)',
                'rgba(95, 158, 160, 0.8)',
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
var weekly = document.getElementById("weekly_chart").getContext('2d');
var weekly_data = {
    labels: [
        "Device 1",
        "Device 2",
        "Device 3",
    ],
    datasets: [
        {
            label: 'Weekly report',
            data: [1, 1, 0],
            backgroundColor: [
                'rgba(175, 238, 38, 0.8)',
                'rgba(64, 224, 208, 0.8)',
                'rgba(95, 158, 160, 0.8)',
            ],

        }]
};

var pieChart = new Chart(weekly, {
    type: 'pie',
    data: weekly_data
});